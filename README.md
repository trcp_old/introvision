# An Introduction to Robot Vision

We present a short introduction to Robot Vision. We first present the basic concepts of image publishers and subscribers in ROS and then we apply some basic commands to introduce the students to the digital image processing theory; finally, we present some RGBD and point cloud notions and applications.

# Prerequisites

What things you need to install the software and how to install them

```
You should have ROS installed.
You should have OpenCV and OpenNI installed.
You should have a working ROS workspace.
```
# Installation

## 0. Requirements

Install git:

```
sudo apt-get install git
```

Install wstool:

```
sudo apt-get install python-wstool
```

## 1. ROS Install:

### Ubuntu 16.04:

Follow the indications here:

```
http://wiki.ros.org/kinetic/Installation/Ubuntu
```

### Ubuntu 18.04:

Fllow the indications here:
```
http://wiki.ros.org/melodic/Installation/Ubuntu
```

## 2. Install OpenCV:

### Ubuntu 16.04:

Follow the indications here:

```
https://github.com/roboticslab-uc3m/installation-guides/blob/master/install-opencv.md
```

### Ubuntu 18.04:

If you have a GPU card, first, install CUDA 9 as described here:

```
https://www.pugetsystems.com/labs/hpc/How-to-install-CUDA-9-2-on-Ubuntu-18-04-1184/
```

Then, if your computer supports CUDA 10, follow these indications:

```
https://www.pugetsystems.com/labs/hpc/How-To-Install-CUDA-10-together-with-9-2-on-Ubuntu-18-04-with-support-for-NVIDIA-20XX-Turing-GPUs-1236/
```

Finally, install OpenCV as in here:

```
https://github.com/roboticslab-uc3m/installation-guides/blob/master/install-opencv.md
```

** Alternatively, to install OpenCV, you can follow these instructions:

```
https://demura.net/misc/16485.html
```

## 3. Install OpenNI/NITE:

Follow the instructions here (both Ubuntu 16.04 and 18.04):

```
https://github.com/roboticslab-uc3m/installation-guides/blob/master/install-openni-nite.md
```

### 3.1 Install OpenNI in ROS:

### Ubuntu 16.04:

Follow the indications here:

```
sudo apt-get install ros-kinetic-openni*
```

### Ubuntu 18.04:

Proceed as indicated in the following link:

```
sudo apt-get install ros-melodic-openni*
```

## 4. Get the introduction to robot vision libraries:

### 4.1 Clone this repository

First, create a workspace:

```
cd ~
mkdir -p erasers_ws/src
cd erasers_ws
catkin_make
```

Then, clone this repository into the src folder:

```
cd ~/erasers_ws/src
git clone https://gitlab.com/trcp/introvision.git
cd ..
catkin_make
```

### 4.2 Test the code

Run the following command in a terminal:

```
roscore
```

In a second terminal, run these commands:

```
source ~/erasers_ws/devel/setup.bash
rosrun introvision_images my_publisher ~/erasers_ws/src/introvision/1_images/data/baboon.png
```


Then, in a third terminal, run these commands:

```
source ~/erasers_ws/devel/setup.bash
rosrun introvision_images my_subscriber
```

# Developing

Basic concepts on ROS image publishers and subscribers can be found here:

> https://gitlab.com/trcp/introvision/-/tree/master/1_images

To learn how process RGB images, follows the indications here:

> https://gitlab.com/trcp/introvision/-/tree/master/2_processing

To work with RGBD images, enter here:

> https://gitlab.com/trcp/introvision/-/tree/master/3_rgbd

# Authors

Please, if you use this material, don't forget to add the following reference:

```
@misc{contreras2020,
    title={An introduction to Robot Vision},
    author={Luis Contreras and Hiroyuki Okada},
    url={https://gitlab.com/trcp/introvision},
    year={2020}
}
```

* **Luis Contreras** - [AIBot](http://aibot.jp/)
* **Hiroyuki Okada** - [AIBot](http://aibot.jp/)
